﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public enum BannerSize
{
    Banner,
    MediumRectangle,
    IABBanner,
    Leaderboard,
    SmartBanner,
}
public class GoogleAds : MonoBehaviour
{
    [Header("Banner Infor")]
    public string BannerId;
    public AdPosition BannerPosition = AdPosition.Top;
    public BannerSize BannerSize = BannerSize.Banner;
    private BannerView bannerView;
    private AdSize adSize;

    [Header("Full Infor")]
    public string FullId;
    private InterstitialAd interstitial;

    [Header("Video Infor")]
    public string VideoId;
    private RewardBasedVideoAd rewardBasedVideo;

    public delegate void AdsClickDelegete();
    public AdsClickDelegete OnBannerClick;
    public AdsClickDelegete OnFullClick;
    public AdsClickDelegete OnVideoClick;

    void Awake()
    {
        GetSize();

		// Initialize an RewardBasedVideoAd.
		rewardBasedVideo = RewardBasedVideoAd.Instance;
		rewardBasedVideo.OnAdClosed += (sender, args) => RequestRewardBasedVideo();
		rewardBasedVideo.OnAdLeavingApplication += (sender, args) => OnVideoClick();
    }

    void Start()
    {
        RequestBanner();
        RequestInterstitial();
        RequestRewardBasedVideo();
    }	 

    private void GetSize()
    {
        switch (BannerSize)
        {
            case BannerSize.Banner:
                adSize = AdSize.Banner;
                break;
            case BannerSize.IABBanner:
                adSize = AdSize.IABBanner;
                break;
            case BannerSize.Leaderboard:
                adSize = AdSize.Leaderboard;
                break;
            case BannerSize.MediumRectangle:
                adSize = AdSize.MediumRectangle;
                break;
            case BannerSize.SmartBanner:
                adSize = AdSize.SmartBanner;
                break;
        }
    }

    private void RequestBanner()
    {
		// Create a banner at the top of the screen.
		bannerView = new BannerView(BannerId, adSize, BannerPosition);
		bannerView.OnAdLeavingApplication += (sender, args) => OnBannerClick();

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public void ShowBanner()
    {
        bannerView.Show();
    }

    public void HideBanner()
    {
        bannerView.Hide();
    }

    private void RequestInterstitial()
    {
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(FullId);
		interstitial.OnAdClosed += (sender, args) => RequestInterstitial();
		interstitial.OnAdLeavingApplication += (sender, args) => OnFullClick();

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request. 
        interstitial.LoadAd(request);
    }

    public void ShowFull()
    {
        if (interstitial.IsLoaded())
            interstitial.Show();
        else
            RequestInterstitial();
    }

    private void RequestRewardBasedVideo()
	{
		AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, VideoId);
    }

    public void ShowVideo()
    {
        if (rewardBasedVideo.IsLoaded())
            rewardBasedVideo.Show();
        else
            RequestRewardBasedVideo();
    }
}
